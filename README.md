# Sudoku Solver

* URL: https://www.codewars.com/kata/5296bc77afba8baa690002d7

this was my first attempt ever at solving a Sudoku puzzle _programmatically_... so yes, I expect the code is still very "rough". that's all part of the learning process. (:

I passed the original Codewars Kata ages back with only the first "reduce cell options" solver logic, including an "easy" one from my favourite Android game... but I couldn't get it to solve the "medium", "hard" or "expert" puzzles from the same game, whereas I could solve them myself.

this was not acceptible.. and so, here we are. :D
