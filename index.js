const { exit } = require("process");
const { readFileSync } = require("fs");
const { resolve } = require("path");
const sudokuSolver = require("./src/solution.js");

const puzzleName = process.argv[2];
if (!puzzleName) {
  console.error("no puzzle selected.. usage example: 'node index.js 2-easy'");
  exit(1);
}

let selectedPuzzleBoard;
let knownPuzzleSolution;

try {
  selectedPuzzleBoard = JSON.parse(readFileSync(resolve(`./puzzles/${puzzleName}.json`)));
} catch (error) {
  console.log("failed to load puzzle source:", error.message);
  exit(1);
}

try {
  knownPuzzleSolution = JSON.parse(readFileSync(resolve(`./solutions/${puzzleName}.json`)));
} catch (error) {
  console.log("failed to load puzzle solution:", error.message);
  exit(1);
}

try {
  console.log("---------------------------------------------------");

  console.log("starting puzzle board:", selectedPuzzleBoard.map(JSON.stringify));
  sudokuSolver(selectedPuzzleBoard);
  console.log("updated puzzle board:", selectedPuzzleBoard.map(JSON.stringify));

  const solutionIsValid = JSON.stringify(selectedPuzzleBoard) === JSON.stringify(knownPuzzleSolution);
  console.log("solution is correct:", solutionIsValid);
  if (!solutionIsValid) {
    console.log("expected solution:", knownPuzzleSolution.map(JSON.stringify));
  }

  console.log("---------------------------------------------------");
} catch (error) {
  console.error("unexpected error while executing solution:", error);
}
