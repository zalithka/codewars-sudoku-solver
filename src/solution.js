/**
 * this function takes an unsolved Sudoku as its only argument, solving the puzzle in-place.
 *
 * @param {Number[][]} puzzle is a 2D array of 9x9 single digits
 */
function sudoku(puzzle) {
  // prepare some variables for tracking solution progress..
  const maxLoopyCount = 10;
  let loopyCount = 0;
  let previouslyUnsolvedCells;

  // first flatten the puzzle to a single array including cell locations
  const celldata = extractCellDataFromPuzzle(puzzle);
  let unsolvedCells = countUnsolvedCells(celldata);

  while (unsolvedCells !== 0 && loopyCount < maxLoopyCount) {
    loopyCount++;
    previouslyUnsolvedCells = unsolvedCells;
    console.log(`starting iteration ${loopyCount} with ${unsolvedCells} cells to solve..`);

    // now run each of the logic based solvers on the array of puzzle cell metadata..
    celldata.forEach((cell) => reduceCellOptionsSolver(celldata, cell));
    celldata.forEach((cell) => checkUniqueOptionValueSolver(celldata, cell));
    celldata.forEach((cell) => identicalOptionsSolver(celldata, cell));
    celldata.forEach((cell) => markUniqueBlockOptionsSolver(celldata, cell));

    // update the "unsolved cells" count
    unsolvedCells = countUnsolvedCells(celldata);

    if (unsolvedCells === previouslyUnsolvedCells) {
      console.warn("=== solution might be stuck, no cells were solved this iteration.. ===");
    }
  }

  console.log(`solver loop quit at ${loopyCount} iterations with ${unsolvedCells} unsolved cells..`);

  // update the provided puzzle board with the calculated values..
  updatePuzzleUsingCellData(puzzle, celldata);
}

/**
 * this converts the 2D puzzle array into a flat array of cell positions and potential values..
 *
 * @param {Number[][]} puzzle is a 2D array of 9x9 single digits
 * @returns {{loc: [Number, Number], opts: Number[], excl: Number[]}[]} an array of cell celldata objects
 */
function extractCellDataFromPuzzle(puzzle) {
  return puzzle.reduce((list, row, rIndex) => {
    const cellRowData = row.map((cell, cIndex) => {
      const opts = cell === 0 ? [1, 2, 3, 4, 5, 6, 7, 8, 9] : [cell];
      const excl = [1, 2, 3, 4, 5, 6, 7, 8, 9].filter((o) => !opts.includes(o));
      return {
        loc: [rIndex, cIndex],
        opts,
        excl,
      };
    });
    return list.concat(cellRowData);
  }, []);
}

/**
 * this updates the 2D puzzle array using values from the flat array of cell positions and solved values.
 *
 * explicitly passing 'null' for 'puzzle' will adjust the internal logic to return a board designed for debugging.
 *
 * @param {Number[][] | null} puzzle is a 2D array of 9x9 single digits
 * @param {{loc: [Number, Number], opts: Number[], excl: Number[]}[]} celldata an array of cell celldata objects
 * @returns a 2D array of 9x9 single digits
 */
function updatePuzzleUsingCellData(puzzle, celldata) {
  return celldata.reduce((p, cell) => {
    const cellValue = cell.opts.length === 1 ? cell.opts[0] : cell.opts;
    p[cell.loc[0]][cell.loc[1]] = cellValue;
    return p;
  }, puzzle);
}

/**
 * count the total number of cells that are still to be solved
 *
 * @param {{loc: [Number, Number], opts: Number[], excl: Number[]}[]} celldata an array of cell celldata objects
 * @returns Number as a count of unsolved cells
 */
function countUnsolvedCells(celldata) {
  return celldata.filter((cell) => cell.opts.length > 1 || cell.opts.includes(0)).length;
}

/**
 * update target cell list of options by excluding all solved sibling cell values
 *
 * @param {{loc: [Number, Number], opts: Number[], excl: Number[]}[]} celldata
 * @param {{loc: [Number, Number], opts: Number[], excl: Number[]}} cell
 */
function reduceCellOptionsSolver(celldata, cell) {
  if (cell.opts.length > 1) {
    // console.log(`--- reduceCellOptionsSolver ---`);
    // console.log(`cell: ${JSON.stringify(cell)}`);

    const solvedSiblingCellValues = [
      ...getRowSiblings(celldata, cell),
      ...getColumnSiblings(celldata, cell),
      ...getBlockSiblings(celldata, cell),
    ]
      .filter((c) => c.opts.length === 1)
      .map((c) => c.opts[0]);
    const allImpossibleValues = [...solvedSiblingCellValues, ...cell.excl].reduce((u, v) => {
      if (!u.includes(v)) u.push(v);
      return u;
    }, []);

    const newCellOptions = [1, 2, 3, 4, 5, 6, 7, 8, 9].filter((o) => !allImpossibleValues.includes(o));
    if (newCellOptions.length < cell.opts.length) {
      cell.opts = newCellOptions;
      allImpossibleValues.sort();
      cell.excl = allImpossibleValues;
      if (newCellOptions.length === 1) {
        refreshSolvedCellSiblings(celldata, cell);
      }
    }
  }
}

/**
 * this checks to see if any option in the target cell is unique in any of it's sibling groups.
 *
 * any unique option in any group results in the cell being solved by that option.
 *
 * @param {{loc: [Number, Number], opts: Number[], excl: Number[]}[]} celldata
 * @param {{loc: [Number, Number], opts: Number[], excl: Number[]}} cell
 */
function checkUniqueOptionValueSolver(celldata, cell) {
  if (cell.opts.length > 1) {
    // console.log(`--- checkUniqueOptionSolver ---`);
    // console.log(`cell: ${JSON.stringify(cell)}`);

    const rowSiblings = getRowSiblings(celldata, cell);
    const columnSiblings = getColumnSiblings(celldata, cell);
    const blockSiblings = getBlockSiblings(celldata, cell);

    cell.opts.forEach((option) => {
      // now see if this option exists in any of the sibling groups
      const uniqueInRow = rowSiblings.findIndex((c) => c.opts.includes(option)) < 0;
      const uniqueInColumn = columnSiblings.findIndex((c) => c.opts.includes(option)) < 0;
      const uniqueInBlock = blockSiblings.findIndex((c) => c.opts.includes(option)) < 0;
      if (uniqueInRow || uniqueInColumn || uniqueInBlock) {
        cell.opts = [option];
        cell.excl = [1, 2, 3, 4, 5, 6, 7, 8, 9].filter((o) => o !== option);
        refreshSolvedCellSiblings(celldata, cell);
      }
    });
  }
}

/**
 * for each individual sibling group, if identical siblings are found, excluded these options from all relevant siblings
 *
 * - this only works for cells with 2 or more options
 * - the number of matches must equal the number of options
 * - all matches must be within the same sibling group
 *
 * @param {{loc: [Number, Number], opts: Number[], excl: Number[]}[]} celldata
 * @param {{loc: [Number, Number], opts: Number[], excl: Number[]}} cell
 */
function identicalOptionsSolver(celldata, cell) {
  if (cell.opts.length > 1) {
    // console.log(`--- identicalOptionsSolver ---`);
    // console.log(`cell: ${JSON.stringify(cell)}`);

    const rowSiblings = getRowSiblings(celldata, cell);
    const columnSiblings = getColumnSiblings(celldata, cell);
    const blockSiblings = getBlockSiblings(celldata, cell);

    const identicalRowSiblings = rowSiblings.filter((c) => JSON.stringify(c.opts) === JSON.stringify(cell.opts));
    const identicalColumnSiblings = columnSiblings.filter((c) => JSON.stringify(c.opts) === JSON.stringify(cell.opts));
    const identicalBlockSiblings = blockSiblings.filter((c) => JSON.stringify(c.opts) === JSON.stringify(cell.opts));

    const otherRowSiblings = rowSiblings.filter((c) => !identicalRowSiblings.includes(c));
    const otherColumnSiblings = columnSiblings.filter((c) => !identicalColumnSiblings.includes(c));
    const otherBlockSiblings = blockSiblings.filter((c) => !identicalBlockSiblings.includes(c));

    const requiredCount = cell.opts.length - 1;

    if (identicalRowSiblings.length === requiredCount) {
      cell.opts.forEach((option) => {
        otherRowSiblings.forEach((c) => makeOptionImpossible(c, option));
      });
    }

    if (identicalColumnSiblings.length === requiredCount) {
      cell.opts.forEach((option) => {
        otherColumnSiblings.forEach((c) => makeOptionImpossible(c, option));
      });
    }

    if (identicalBlockSiblings.length === requiredCount) {
      cell.opts.forEach((option) => {
        otherBlockSiblings.forEach((c) => makeOptionImpossible(c, option));
      });
    }
  }
}

/**
 * see if any of the target cell options are only valid in the current block of the relevant "row" or "column"
 *
 * if so, then:
 * - the remaining blocks may not have that value in the same "row" or "column"
 * - the current block may not have that value in the other "rows" or "columns"
 *
 * @param {{loc: [Number, Number], opts: Number[], excl: Number[]}[]} celldata
 * @param {{loc: [Number, Number], opts: Number[], excl: Number[]}} cell
 */
function markUniqueBlockOptionsSolver(celldata, cell) {
  if (cell.opts.length > 1) {
    // console.log(`--- markUniqueBlockOptionsSolver ---`);
    // console.log(`cell: ${JSON.stringify(cell)}`);

    const rowSiblings = getRowSiblings(celldata, cell); //.filter((c) => c.opts.length > 1);
    const columnSiblings = getColumnSiblings(celldata, cell); //.filter((c) => c.opts.length > 1);
    const blockSiblings = getBlockSiblings(celldata, cell); //.filter((c) => c.opts.length > 1);

    const otherBlockRowSiblings = rowSiblings.filter((c) => !blockSiblings.includes(c));
    const otherBlockColumnSiblings = columnSiblings.filter((c) => !blockSiblings.includes(c));

    const otherRowBlockSiblings = blockSiblings.filter((c) => !rowSiblings.includes(c));
    const otherColumnBlockSiblings = blockSiblings.filter((c) => !columnSiblings.includes(c));

    cell.opts.forEach((option) => {
      const uniqueToBlockRow = otherBlockRowSiblings.filter((s) => s.opts.includes(option)).length === 0;
      if (uniqueToBlockRow) {
        otherRowBlockSiblings.forEach((c) => makeOptionImpossible(c, option));
      }

      const uniqueToRowBlock = otherRowBlockSiblings.filter((s) => s.opts.includes(option)).length === 0;
      if (uniqueToRowBlock) {
        otherBlockRowSiblings.forEach((c) => makeOptionImpossible(c, option));
      }

      const uniqueToBlockColumn = otherBlockColumnSiblings.filter((s) => s.opts.includes(option)).length === 0;
      if (uniqueToBlockColumn) {
        otherColumnBlockSiblings.forEach((c) => makeOptionImpossible(c, option));
      }

      const uniqueToColumnBlock = otherColumnBlockSiblings.filter((s) => s.opts.includes(option)).length === 0;
      if (uniqueToColumnBlock) {
        otherBlockColumnSiblings.forEach((c) => makeOptionImpossible(c, option));
      }
    });
  }
}

function makeOptionImpossible(cell, option) {
  if (cell.opts.includes(option)) {
    cell.opts.splice(cell.opts.indexOf(option), 1);
  }
  if (!cell.excl.includes(option)) {
    cell.excl.push(option);
    cell.excl.sort();
  }
}

/**
 *
 * @param {{loc: [Number, Number], opts: Number[], excl: Number[]}[]} celldata an array of cell celldata objects
 * @param {{loc: [Number, Number], opts: Number[], excl: Number[]}} cell is the cell to use as a location anchor
 * @returns {{loc: [Number, Number], opts: Number[], excl: Number[]}[]} celldata an array of cell celldata objects
 */
function getRowSiblings(celldata, cell) {
  return celldata.filter((c) => {
    return cell.loc !== c.loc && cell.loc[0] == c.loc[0];
  });
}

/**
 *
 * @param {{loc: [Number, Number], opts: Number[], excl: Number[]}[]} celldata an array of cell celldata objects
 * @param {{loc: [Number, Number], opts: Number[], excl: Number[]}} cell is the cell to use as a location anchor
 * @returns {{loc: [Number, Number], opts: Number[], excl: Number[]}[]} celldata an array of cell celldata objects
 */
function getColumnSiblings(celldata, cell) {
  return celldata.filter((c) => {
    return cell.loc !== c.loc && cell.loc[1] == c.loc[1];
  });
}

/**
 *
 * @param {{loc: [Number, Number], opts: Number[], excl: Number[]}[]} celldata an array of cell celldata objects
 * @param {{loc: [Number, Number], opts: Number[], excl: Number[]}} cell is the cell to use as a location anchor
 * @returns {{loc: [Number, Number], opts: Number[], excl: Number[]}[]} celldata an array of cell celldata objects
 */
function getBlockSiblings(celldata, cell) {
  const targetCellBlock = [Math.floor(cell.loc[0] / 3), Math.floor(cell.loc[1] / 3)];
  return celldata.filter((c) => {
    const siblingCellBlock = [Math.floor(c.loc[0] / 3), Math.floor(c.loc[1] / 3)];
    return cell.loc !== c.loc && JSON.stringify(targetCellBlock) === JSON.stringify(siblingCellBlock);
  });
}

/**
 * this updates all siblings of the target cell, adjusting the "opts" and "excl" list of each one as needed.
 *
 * @param {{loc: [Number, Number], opts: Number[], excl: Number[]}[]} celldata
 * @param {{loc: [Number, Number], opts: Number[], excl: Number[]}} cell
 */
function refreshSolvedCellSiblings(celldata, cell) {
  const rowSiblings = getRowSiblings(celldata, cell);
  const columnSiblings = getColumnSiblings(celldata, cell);
  const blockSiblings = getBlockSiblings(celldata, cell).filter(
    (c) => ![...rowSiblings, ...columnSiblings].includes(c)
  );

  const allSiblings = [...rowSiblings, ...columnSiblings, ...blockSiblings];

  const cellSolution = cell.opts[0];
  allSiblings.map((c) => {
    const oIndex = c.opts.findIndex((o) => o === cellSolution);
    if (oIndex !== -1) {
      c.opts.splice(oIndex, 1);
      c.opts.sort();
    }
    const xIndex = c.excl.findIndex((o) => o === cellSolution);
    if (xIndex === -1) {
      c.excl.push(cellSolution);
      c.excl.sort();
    }
  });
}

//! the following is not required for Codewars, this is just to enable my local runtime wrapper to do it's thing..
module.exports = sudoku;
